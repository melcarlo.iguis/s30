const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Syntax for connecting database 
	// mongoose.connect('<connection string , {middleware}')
mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.kkyuv.mongodb.net/session30?retryWrites=true&w=majority' , 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
)

let db = mongoose.connection;

	// console.error.bind - print error in the browser and in the terminal
	db.on("error", console.error.bind(console , "Connection error"))

	// If the connection is successful , this will 
	db.once('open' , ()=> console.log("Connected to the cloud database"))

// Mongo Schema
const taskSchema = new mongoose.Schema({

	// name of our schema - taskSchema
	// new mongoose.Schema - is a method to make Schema
	// we will be needing the name of the task and its status
	// each field will required a data type

	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});

const Task = mongoose.model('Task', taskSchema);
	// models use schema and they act as the midleman from the server to our database

	// Model can now be used to run commands for interacting with our database

	// naming convention - name of model should be capitalized and singular form ' Task'

	// 2nd parameter - is used to specify the schema of the documents that will be restored in the mongoDB collection

// Business Logic
	/*
		1. add functionality - to check if there are duplicate tasks 
			-if the task already exist in the db, we return an error -else we will add it to our database

		2. The task data will be coming from the request body.
		3.Create a new Task object with name field property
	*/

app.post('/tasks' , (req, res) =>{

	Task.findOne({name: req.body.name}, (err,result)=>{

		if(result != null && result.name === req.body.name){

			return res.send('Duplicate task found')
		}
		else{
			let newTask = new Task({

				name: req.body.name
			})

			newTask.save((saveErr, saveTask)=>{
				if(saveErr){
					return console.log(saveErr);
				}else{
					return res.status(201).send('New Task Created')
				}
			})
		}
	})
})


// Retrieving all task

app.get('/tasks', (req,res) => {

	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})


// ACTIVITY CODE

// 	Create a User Schema
const userSchema = new mongoose.Schema({

	username: String,
	password: String,
	
});

// Create a User Model
const User = mongoose.model('User', userSchema);


// Create a POST route to register 3 users.
app.post('/signup' , (req, res) =>{

	User.findOne({username: req.body.username}, (err,result)=>{

		if(result != null && result.username === req.body.username){
			return res.send('User already exist')
		}
		else{
			let newUser = new User({

				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, saveTask)=>{
				if(saveErr){
					return console.log(saveErr);
				}else{
					return res.status(201).send('Sucessfully Registered')
				}
			})
		}
	})
})

// Retrieve all User
app.get('/users', (req,res) => {

	User.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))